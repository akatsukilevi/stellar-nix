{ ... }:
{
  imports =
  [
    # Common config
    ../common/nixos.nix
    ../common/bootloader.nix
    ../common/locale.nix
    ../common/networking.nix
    ../common/packages.nix
    ../common/users.nix

    # Hardware config
    ../common/hardware/sound.nix

    # TODO: Add Wayland
  ];

  # Common config
  networking.hostName = "stellar-3a";
  time.timeZone = "America/Sao_Paulo";

  # Keyboard - Can't switch the keyboard because US layouts were never made for this machine
  console.keyMap = "br-abnt2";

  # Base state
  system.stateVersion = "23.11";
}