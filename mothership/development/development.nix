{ pkgs, ... }:
{
  # Add packages for development
  environment.systemPackages = with pkgs; [ vscodium-fhs nil godot_4 ];
}
