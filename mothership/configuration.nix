{ pkgs, inputs, ... }:
{
  imports =
  [
    # Base configuration
    ./hardware-configuration.nix
    
    # Common config
    ../common/nixos.nix
    ../common/bootloader.nix
    ../common/locale.nix
    ../common/networking.nix
    ../common/packages.nix
    ../common/users.nix

    # Hardware config
    ../common/hardware/sound.nix
    ../common/hardware/nvidia.nix
    #./joystick/joystick.nix
    ./ds4/ds4.nix
    ./smfc/smfc.nix

    # Use X11 - Hardware uses legacy parts
    ../common/video/xserver.nix
    
    # Apps
    ./apps/gaming.nix
    ./apps/multimedia.nix
    ./apps/theme.nix
    ./apps/tools.nix
    ./apps/utilities.nix

    # Development
    ./development/development.nix

    # Misc
    inputs.aagl.nixosModules.default
  ];

  # Common config
  networking.hostName = "stellar-mothership";
  time.timeZone = "America/Sao_Paulo";

  # Keyboard
  # TODO: I should switch to a dvorak keyboard when I can
  console.keyMap = "br-abnt2";
  services.xserver.xkb.layout = "br";
  services.xserver.xkb.variant = "";

  # GnuPG
  programs.gnupg.agent.pinentryPackage = pkgs.pinentry-rofi;

  # Base state
  system.stateVersion = "23.11";
}
