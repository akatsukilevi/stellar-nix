{ ... }:
let 
  joystickDevice = "/dev/input/by-id/usb-12bd_USB_Vibration_Joystick-event-joystick";
in
{
  systemd.services = {
    xboxemu = {
      serviceConfig = {
        Type = "simple";
        User = "root";
        Group = "root";
        ExecStart = ["/run/current-system/sw/bin/xboxdrv --evdev ${joystickDevice} --config ${./xboxdrv.conf}"];
      };
      wantedBy = ["multi-user.target"];
    };
  };
}