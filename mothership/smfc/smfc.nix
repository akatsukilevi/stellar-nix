{ pkgs, ... }:
{
  systemd.services = {
    smfc = {
      serviceConfig = {
        Type = "simple";
        User = "root";
        Group = "root";
        ExecStart = ["/run/current-system/sw/bin/python ${./smfc.py} -c ${./smfc.conf} -l 3"];
      };
      wantedBy = ["multi-user.target"];
    };
  };

  # Dependencies
  environment.systemPackages = with pkgs; [
    python3
    hddtemp
    smartmontools
    ipmitool
    ipmicfg
  ];
}