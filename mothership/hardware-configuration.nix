{ config, lib, modulesPath, ... }:
{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot.initrd.availableKernelModules = [ "xhci_pci" "ehci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" "jc42" "tpm_rng" "ipmi_devintf" "ipmi_si" ];
  boot.extraModulePackages = [ ];

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/ade47944-7875-4ad0-8746-7579cf0b1a4b";
      fsType = "ext4";
    };
    "/boot" = {
      device = "/dev/disk/by-uuid/5B56-6EB4";
      fsType = "vfat";
    };
    "/mnt/files" = {
      device = "/dev/disk/by-uuid/b18a92fb-290e-4bab-a20f-d30cf7758002";
      fsType = "ext4";
      options = [
        "defaults"
        "x-gvfs-show"
        "x-gvfs-name=Files"
        "nofail"
      ];
    };
    "/mnt/media" = {
      depends = ["/mnt/files"];
      device = "/mnt/files/Media";
      fsType = "none";
      options = [
        "bind"
        "x-gvfs-show"
        "x-gvfs-name=Media"
        "x-gvfs-icon=folder-documents"
        "x-gvfs-symbolic-icon=folder-documents"
        "nofail"
      ];
    };
    "/mnt/games" = {
      device = "/dev/disk/by-uuid/d45b4fa2-6f9b-41aa-bfaf-75215495723d";
      fsType = "ext4";
      options = [
        "defaults"
        "x-gvfs-show"
        "x-gvfs-name=Games"
        "x-gvfs-icon=applications-games"
        "x-gvfs-symbolic-icon=applications-games"
        "nofail"
      ];
    };
  };

  swapDevices = [
    { device = "/dev/disk/by-uuid/6e9df161-766f-443d-9d35-ae3b3f40d5e9"; }
  ];

  networking.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
