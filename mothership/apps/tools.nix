{ pkgs, ... }:
{
  # Install apps used by this machine
  environment.systemPackages = with pkgs; [
    rofi
    pinentry-rofi
    kitty
    hyfetch
    firefox
    keepassxc
    nextcloud-client
    flameshot
  ];
}
