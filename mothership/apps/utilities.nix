{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    cinnamon.nemo
    cinnamon.nemo-with-extensions
    cinnamon.nemo-emblems
    cinnamon.nemo-fileroller
    cinnamon.folder-color-switcher
    xfce.tumbler
    tor-browser
    adoptopenjdk-jre-hotspot-bin-16
    syncterm
  ];
}
