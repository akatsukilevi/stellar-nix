{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    # Games
    cataclysm-dda
    mindustry
    celeste64

    # Emulators
    cemu

    # Launchers
    (lutris.override {
      extraPkgs = pkgs: [
      	wineWowPackages.staging
      	winetricks
      ];
    })
    steam
    heroic

    # DOOM
    doomrunner
    doomseeker
    zandronum
    gzdoom

    # Quake
    yquake2
    yquake2-ground-zero
    yquake2-ctf
    yquake2-all-games
    openarena

    # Misc
    xboxdrv
  ];

  # Enable AGL
  programs.anime-game-launcher.enable = true;

  # Enable Gamemode
  programs.gamemode.enable = true;
  programs.gamemode.settings.general.inhibit_screensaver = 0;
}
