{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    blender
    krita
  ];
}
