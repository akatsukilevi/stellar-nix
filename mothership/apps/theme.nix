{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    gruvbox-gtk-theme
    capitaine-cursors-themed
    gruvbox-dark-icons-gtk
    kde-gruvbox
  ];
}
