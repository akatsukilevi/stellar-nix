{ pkgs, ... }:
{
  # Those are system-wide utilities, not a per-user package
  environment.systemPackages = with pkgs; [
    # Better replacement for default utilities
    bat
    du-dust
    duf
    ripgrep
    eza
    procs

    # Overall tools
    rsync
    starship
    git
    wget
    gnupg
    pinentry-curses
    pinentry

    # Add-ons
    nanorc
  ];

  # Configure GnuPG
  services.pcscd.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  # Enable flatpak
  services.flatpak.enable = true;
}
