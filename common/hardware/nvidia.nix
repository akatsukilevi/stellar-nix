{ pkgs, config, ... }:
{
  # Enable OpenGL
  hardware.opengl = {
    enable = true;
    driSupport = true;
    driSupport32Bit = true;
  };

  # Mark nvidia license as accepted
  nixpkgs.config.nvidia.acceptLicense = true;

  # Configure nvidia driver
  hardware.nvidia = {
    # Use legacy 470 driver - GT 710
    package = config.boot.kernelPackages.nvidiaPackages.legacy_470;

    # Modesetting is required.
    modesetting.enable = true;

    # Enable the Nvidia settings menu
    nvidiaSettings = true;

    # Enable full composition to fix screen tearing
    forceFullCompositionPipeline = false; # Causing lag
  };

  # Install Vulkan packages
  environment.systemPackages = with pkgs; [
    vulkan-loader
    vulkan-validation-layers
    vulkan-tools
    glxinfo
    inxi
  ];

  # Load nvidia driver for Xorg and Wayland
  services.xserver.videoDrivers = ["nvidia"];

  # Enable nvidia module on init system
  # FIXME: This is causing a package collision, commenting out for now
  #boot.initrd.kernelModules = [ "nvidia" ];
  #boot.extraModulePackages = [ config.boot.kernelPackages.nvidia_x11 ];
}
