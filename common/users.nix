{ ... }:
{
  # Declare main user group
  users.groups.levi = {
    gid = 1000;
  };

  # Declare main user
  users.users.levi = {
    isNormalUser = true;
    home = "/home/levi";
    group = "levi";
    description = "Levi Akatsuki";
    extraGroups = [ "wheel" "networkmanager" "users" ];
  };
}
