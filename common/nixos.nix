{ pkgs, ... }:
{
  # Enable non-free packages
  nixpkgs.config.allowUnfree = true;

  # Enable flakes
  nix = {
    # Enable storage optimization
    optimise.automatic = true;

    settings = {
      # Automatically optimize the storage store
      auto-optimise-store = true;

      # Enable flakes
      experimental-features = ["nix-command" "flakes"];
    };

    # Enable garbage collection
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };
  };

  # Enable LD
  programs.nix-ld.enable = true;
  programs.nix-ld.libraries = with pkgs; [];

  # Register AppImage run support
  boot.binfmt.registrations.appimage = {
    wrapInterpreterInShell = false;
    interpreter = "${pkgs.appimage-run}/bin/appimage-run";
    recognitionType = "magic";
    offset = 0;
    mask = ''\xff\xff\xff\xff\x00\x00\x00\x00\xff\xff\xff'';
    magicOrExtension = ''\x7fELF....AI\x02'';
  };

  # Auto system update
  system.autoUpgrade = {
    enable = true;
  };

  # Replace sudo with doas
  security.doas.enable = true;
  security.sudo.enable = false;
  security.doas.extraRules = [
    {
      groups = ["wheel"];
      setEnv = ["PATH"];
      keepEnv = true; 
      persist = true;
    }
    {
      users = ["root"];
      setEnv = ["PATH"];
      keepEnv = true; 
      noPass = true;
    }
  ];
}
