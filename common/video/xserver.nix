{ pkgs, ... }:
{
  # Enable X11
  services.xserver = {
    enable = true;
    updateDbusEnvironment = true;

    # Enable LigthDM with XFCE
    desktopManager.xfce.enable = true;
    desktopManager.xfce.enableXfwm = true;
    displayManager = {
      defaultSession = "xfce";
      lightdm = {
        enable = true;
        greeters.slick = {
          enable = true;
          draw-user-backgrounds = true;
        };
      };
    };
  };

  # Add plugins for XFCE
  environment.systemPackages = with pkgs; [
    xfce.orage
    xfce.parole
    xfce.gigolo
    xfce.tumbler
    xfce.ristretto
    xfce.mousepad
    gnome.gnome-keyring
    xfce.xfce4-taskmanager
    xfce.xfce4-settings
    xfce.xfce4-whiskermenu-plugin
    xfce.xfce4-windowck-plugin
    xfce.xfce4-pulseaudio-plugin
    xfce.xfce4-sensors-plugin
    xfce.xfce4-cpugraph-plugin
    xfce.xfce4-weather-plugin
    betterlockscreen
  ];

  # Enable general utilities
  services.gvfs.enable = true;
  services.gnome.gnome-keyring.enable = true;

  # Enable XDG Portal
  xdg.portal.enable = true;
  xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
}
